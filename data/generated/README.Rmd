---
output: md_document
---

```{r init, echo = FALSE}

knitr::opts_chunk$set(
  echo = TRUE, eval = TRUE, message = FALSE, warning = FALSE, error = FALSE,
  comment = "|"
)

```

# Simple data

```{r eval.tbl1}

tbl1 <- readr::read_csv("simple.csv")
str(tbl1)

```

This simple data only consider several key information:

- `sex` representing the gender of the victims
- `group` representing the group of perpetrators
- `weapon` representing the weapon used in the violent incidence
- `year` representing the year of occurrence

```{r tbl1}

ftable(year ~ sex, data = tbl1)
ftable(year ~ sex + group, data = tbl1)
ftable(year ~ sex + group + weapon, data = tbl1)

```
