# Simple data

    tbl1 <- readr::read_csv("simple.csv")
    str(tbl1)

    | spc_tbl_ [500 × 4] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    |  $ sex   : chr [1:500] "male" "male" "male" "female" ...
    |  $ group : chr [1:500] "Residivis" "Residivis" "Serikat buruh" "Serikat buruh" ...
    |  $ weapon: chr [1:500] "Benda tajam" "Senjata pelontar" "Senjata pelontar" "Api" ...
    |  $ year  : num [1:500] 2018 2018 2018 2019 2019 ...
    |  - attr(*, "spec")=
    |   .. cols(
    |   ..   sex = col_character(),
    |   ..   group = col_character(),
    |   ..   weapon = col_character(),
    |   ..   year = col_double()
    |   .. )
    |  - attr(*, "problems")=<externalptr>

This simple data only consider several key information:

-   `sex` representing the gender of the victims
-   `group` representing the group of perpetrators
-   `weapon` representing the weapon used in the violent incidence
-   `year` representing the year of occurrence

<!-- -->

    ftable(year ~ sex, data = tbl1)

    |        year 2016 2017 2018 2019 2020
    | sex                                 
    | female         1   19   49  185   10
    | male           0   22   47  160    7

    ftable(year ~ sex + group, data = tbl1)

    |                       year 2016 2017 2018 2019 2020
    | sex    group                                       
    | female Kelompok agama         1    3    8   16    1
    |        Milisi                 0    0    0    1    0
    |        Residivis              0    5   13   58    3
    |        Separatis              0    5    7   32    3
    |        Serikat buruh          0    6   21   78    3
    | male   Kelompok agama         0    2    3   19    0
    |        Milisi                 0    0    0    0    0
    |        Residivis              0    5   24   50    2
    |        Separatis              0    4    7   34    2
    |        Serikat buruh          0   11   13   57    3

    ftable(year ~ sex + group + weapon, data = tbl1)

    |                                           year 2016 2017 2018 2019 2020
    | sex    group          weapon                                           
    | female Kelompok agama Api                         0    1    2    0    1
    |                       Benda tajam                 0    0    0    1    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    0    0    0    0
    |                       Peledak                     0    1    0    4    0
    |                       Senjata api organik         0    0    0    2    0
    |                       Senjata api rakitan         0    0    2    3    0
    |                       Senjata beracun             0    0    1    0    0
    |                       Senjata pelontar            0    1    2    1    0
    |                       Tidak ada                   1    0    1    5    0
    |                       Tidak jelas                 0    0    0    0    0
    |        Milisi         Api                         0    0    0    1    0
    |                       Benda tajam                 0    0    0    0    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    0    0    0    0
    |                       Peledak                     0    0    0    0    0
    |                       Senjata api organik         0    0    0    0    0
    |                       Senjata api rakitan         0    0    0    0    0
    |                       Senjata beracun             0    0    0    0    0
    |                       Senjata pelontar            0    0    0    0    0
    |                       Tidak ada                   0    0    0    0    0
    |                       Tidak jelas                 0    0    0    0    0
    |        Residivis      Api                         0    0    2    7    0
    |                       Benda tajam                 0    0    0    1    0
    |                       Benda tumpul                0    0    0    1    0
    |                       Lainnya                     0    0    0    1    0
    |                       Peledak                     0    0    1    7    2
    |                       Senjata api organik         0    1    2   10    0
    |                       Senjata api rakitan         0    1    2   11    0
    |                       Senjata beracun             0    0    1    1    0
    |                       Senjata pelontar            0    1    3    9    0
    |                       Tidak ada                   0    2    2    9    0
    |                       Tidak jelas                 0    0    0    1    1
    |        Separatis      Api                         0    0    1    2    0
    |                       Benda tajam                 0    0    0    0    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    1    0    2    0
    |                       Peledak                     0    0    2    6    0
    |                       Senjata api organik         0    3    2    3    2
    |                       Senjata api rakitan         0    1    1    4    0
    |                       Senjata beracun             0    0    0    0    0
    |                       Senjata pelontar            0    0    1    6    1
    |                       Tidak ada                   0    0    0    8    0
    |                       Tidak jelas                 0    0    0    1    0
    |        Serikat buruh  Api                         0    1    5    9    0
    |                       Benda tajam                 0    1    0    1    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    0    0    2    0
    |                       Peledak                     0    0    3   12    0
    |                       Senjata api organik         0    0    2   11    0
    |                       Senjata api rakitan         0    2    3   16    0
    |                       Senjata beracun             0    0    0    1    0
    |                       Senjata pelontar            0    1    2   11    2
    |                       Tidak ada                   0    1    6   12    0
    |                       Tidak jelas                 0    0    0    3    1
    | male   Kelompok agama Api                         0    0    1    3    0
    |                       Benda tajam                 0    0    0    1    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    0    0    1    0
    |                       Peledak                     0    1    0    0    0
    |                       Senjata api organik         0    0    0    1    0
    |                       Senjata api rakitan         0    1    1    3    0
    |                       Senjata beracun             0    0    1    0    0
    |                       Senjata pelontar            0    0    0    1    0
    |                       Tidak ada                   0    0    0    8    0
    |                       Tidak jelas                 0    0    0    1    0
    |        Milisi         Api                         0    0    0    0    0
    |                       Benda tajam                 0    0    0    0    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    0    0    0    0
    |                       Peledak                     0    0    0    0    0
    |                       Senjata api organik         0    0    0    0    0
    |                       Senjata api rakitan         0    0    0    0    0
    |                       Senjata beracun             0    0    0    0    0
    |                       Senjata pelontar            0    0    0    0    0
    |                       Tidak ada                   0    0    0    0    0
    |                       Tidak jelas                 0    0    0    0    0
    |        Residivis      Api                         0    1    1    4    1
    |                       Benda tajam                 0    0    1    2    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    0    1    0    0
    |                       Peledak                     0    2    3    6    0
    |                       Senjata api organik         0    0    4   12    0
    |                       Senjata api rakitan         0    1    3    7    1
    |                       Senjata beracun             0    0    1    2    0
    |                       Senjata pelontar            0    1    6   10    0
    |                       Tidak ada                   0    0    4    6    0
    |                       Tidak jelas                 0    0    0    1    0
    |        Separatis      Api                         0    0    0    7    0
    |                       Benda tajam                 0    0    0    0    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    0    0    0    0
    |                       Peledak                     0    0    2    4    0
    |                       Senjata api organik         0    2    0    3    1
    |                       Senjata api rakitan         0    1    2    3    1
    |                       Senjata beracun             0    0    1    5    0
    |                       Senjata pelontar            0    1    1    3    0
    |                       Tidak ada                   0    0    1    9    0
    |                       Tidak jelas                 0    0    0    0    0
    |        Serikat buruh  Api                         0    4    2   11    0
    |                       Benda tajam                 0    0    0    1    0
    |                       Benda tumpul                0    0    0    0    0
    |                       Lainnya                     0    1    0    4    0
    |                       Peledak                     0    1    1    7    0
    |                       Senjata api organik         0    1    3   12    1
    |                       Senjata api rakitan         0    0    3    7    1
    |                       Senjata beracun             0    0    0    0    0
    |                       Senjata pelontar            0    1    4    7    1
    |                       Tidak ada                   0    3    0    7    0
    |                       Tidak jelas                 0    0    0    1    0
